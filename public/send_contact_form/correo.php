<?php

require_once 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
require_once 'vendor/hundredminds/php-validation/Validator.php';
// require_once 'vendor/plasticbrain/php-flash-messages/src/FlashMessages.php';

/** Constantes */
define('SUCESS_MESSAGE', '¡Gracias por enviarnos tu consulta! Nos comunicaremos a la brevedad.');
define('FORM_ERROR_MESSAGE', 'Por favor, complete los campos requeridos de manera correcta.');
define('SEND_ERROR_MESSAGE', 'Error al enviar el mensaje. Por favor intente luego.');
define('EMAIL_SUBJECT', 'Contacto desde landing');
define('EMAIL_FROM', 'nicolas@25watts.com.ar');
define('EMAIL_TO', 'nicolas@25watts.com.ar');

/** enviar mail */
if ($_SERVER["REQUEST_METHOD"] == "POST" && empty($_POST['v'])) {
    $isAjax = 0;
    $formSubmit = 1;
    $response = array(
        "status" => 1,
        "msg" => SUCESS_MESSAGE
    );

    // seteo llamada ajax
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $isAjax = 1;
    }

    // instanciar validator
    $validator = new Validator();

    // pasar datos post a validator
    $validator->setData($_POST);

    // establecer campos y validaciones
    $validator
        ->required('Campo requerido')
        ->validate('nombre');
    $validator
        ->required('Campo requerido')
        ->email('Ingrese un email válido')
        ->validate('email');
    $validator
        ->required('Campo requerido')
        ->validate('tel');
    $validator
        ->required('Campo requerido')
        ->validate('mensaje');

    // validación de form
    $errors = $validator->hasErrors();

    // devuelvo con errores en caso de haber
    if ($errors) {
        $response = array(
            "status" => 0,
            "msg"    => FORM_ERROR_MESSAGE,
            'errors' => $validator->getAllErrors(),
        );
    }
    // validación correcta, proceso formulario
    else {
        $data       = $validator->getValidData();
        $dataLabels = array(
            'nombre' => 'Nombre',
            'email' => 'E-mail',
            'tel' => 'Teléfono',
            'mensaje' => 'Mensaje',
        );
        $mensaje = '';

        foreach ($data as $key => $value) {
            $mensaje .= $dataLabels[$key] . ': ' . $value . '<br>';
        }

        // instancia PHPMailer para enviar mail
        $mail = new PHPMailer;

        // smtp config
        // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only

        // $mail->IsSMTP();
        // $mail->SMTPDebug  = 0;
        // $mail->SMTPAuth   = true;
        // $mail->SMTPSecure = "tls";
        // $mail->Host       = "smtp.gmail.com";
        // $mail->Port       = 587;
        // $mail->Username   = "dev@25watts.com.ar";
        // $mail->Password   = "d3v.2SWatts";

        $mail->CharSet = 'UTF-8';
        $mail->setFrom(EMAIL_FROM);
        $mail->addReplyTo(EMAIL_FROM);
        $mail->addAddress(EMAIL_TO);
        $mail->isHTML(true);

        $mail->Subject = EMAIL_SUBJECT;
        $mail->Body    = $mensaje;
        $mail->AltBody = $mensaje;

        // envío mail, si no se envió devuelvo errores
        if(!$mail->send()) {
            $response = array(
                "status" => 0,
                "msg" => SEND_ERROR_MESSAGE
            );
        }
    }

    // si es ajax retorno json con datos
    if ($isAjax) {
        header('Content-type: application/json');
        echo json_encode($response);
        exit();
    }


    // si NO es ajax redirecciono
    if ($response['status']) {
        header('Location: ../gracias.html');
    } else {
        header('Location: ../error.html');
    }
}
