# Script de env�o de mail

El archivo correo.php es el que produce el env�o utilizando php-validation [https://github.com/blackbelt/php-validation] para validar y phpmailer [https://github.com/PHPMailer/PHPMailer/] para el env�o.

Enviando el formulario a correo.php ya se encarga de validar y enviar. Hay que modificar los campos del formulario y las validaciones seg�n el caso.

# Validaci�n js

p�gina oficial del plugin con documentaci�n [http://jqueryvalidation.org/]

# Env�o ajax de form

p�gina oficial en [http://malsup.com/jquery/form/#options-object]