KEEPER = {
    x: 50,
    y: 310
};
SWEEPER = {
    x: 20,
    y: 25
};
CENTER_BACK_RIGHT = {
    x: 200,
    y: 400
};
CENTER_BACK_LEFT = {
    x: 200,
    y: 250
};
FULLBACK_LEFT = {
    x: 250,
    y: 100
};
STOPPER = {
    x: 20,
    y: 25
};
FULLBACK_RIGHT = {
    x: 250,
    y: 550
};
DEFENSIVE_MIDFIELDER = {
    x: 400,
    y: 310
};
CENTER_MIDFIELDER = {
    x: 20,
    y: 25
};
LEFT_MIDFIELDER = {
    x: 440,
    y: 150
};
RIGHT_MIDFIELDER = {
    x: 440,
    y: 480
};
ATTACKING_MIDFIELDER = {
    x: 20,
    y: 25
};
LEFT_FORWARD = {
    x: 650,
    y: 100
};
RIGHT_FORWARD = {
    x: 650,
    y: 500
};
SECOND_STRIKER = {
    x: 20,
    y: 25
};
STRIKER = {
    x: 750,
    y: 315
};
