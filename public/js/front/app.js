let selected = [];

let base_url = 'https://mundial.templodelfutbol.com.ar/';

//let base_url = 'https://mundial.local.25watts.com.ar/';

class Circle
{

    constructor(context)
    {
        this.context = context;
        this.color = "#FFF";
        this.radio = 65;
    }

    position(position) {
        this.positionName = position;
        this.x = position.x;
        this.y = position.y;
        return this;
    }

    name(value) {
        this.name = value
    }

    draw() {

        this.group = this.context.group();

        this.group
            .attr({
                'width': 100,
                'height': 100
            })
            .animate(400, '<>')
            .translate(this.x, this.y);

        this.circle = this.group
            .circle(this.radio)
            .attr('class', 'jugador')
            /*.stroke({
                color: '#c7d51f',
                width: 2
            })*/
            .fill(this.color);

        let self = this;

        this.text = this.group.text('').move(0,70);

        this.image = this.group.image('');

        this.group.click(function () {
            self.dropModal();
        });

        this.setBackImage();

        return this;
    }

    setBackImage()
    {
        let image = this.group.image();

        this.image.load(this.positionName == KEEPER ? 'images/arquero.png' : 'images/jugador.png')
            .attr({
                'width': 30,
                'height': 50,
                'x': 17,
                'y': 5
            })
            .style('cursor', 'pointer');

    }

    setImage(self, value) {

        Circle.toDataURL(value, function(dataUrl) {
            self.image.load(dataUrl)
                .attr({
                    'height': 66,
                    'width': 47,
                    'x': 9,
                    'y': 3
                })
                .animate(500);
        });


    }

    static toDataURL(url, callback) {

        let xhr = new XMLHttpRequest();
        xhr.onload = function () {
            let reader = new FileReader();
            reader.onloadend = function () {

                //stop loading
                $('#loading').hide();
                callback(reader.result);
            };
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();

    }

    setName(self,name) {
        self.text.text(name === 'Acunia' ? 'Acuna' : name).fill('#FFF');
        self.name = name === 'Acunia' ? 'Acuna' : name;
    }

    getName()
    {
        return this.name;
    }

    move(position) {
        this.group.animate(300, '<>').move(position.x, position.y);
    }

    dropModal() {

        let modal = new Modal();

        modal.drop(this);

    }
}

class Modal {

    constructor()
    {
        this.element = $('#chose');
        this.selected = [];
    }

    drop(player) {
        //this.element.style.display = 'block';
        this.element.modal();

        let choses = window.document.getElementsByClassName('chose-player');

        let self = this;

        for(let i = 0; i < choses.length; i++) {
            let chose = choses[i];
            chose.onclick = function(event) {

                $(chose)
                    .removeAttr('href')
                    .css('opacity', 0.5)
                    .find('.avatar')
                    .css('border', '4px solid #CCC');

                event.preventDefault();

                if (selected.indexOf(player.getName()) >= 0) {
                    selected.splice(selected.indexOf(player.getName()), 1);
                }

                if (selected.indexOf(chose.dataset.name) === -1) {
                    player.image.load('');
                    player.setName(player, chose.dataset.name);

                    player.setImage(player, base_url + 'jugadores/' + chose.dataset.name.replace(/ /g,'') + '.png');

                    selected.push(player.getName());
                }

                if (selected.length === 11)
                {
                    $('#modal-share').modal('show');
                }

                self.element.modal('hide');
            }
        }

        return this;
    }

    hide() {
        this.element.style.display = 'none';
        return this;
    }

}

class Player extends Circle
{
    constructor(nombre, context) {
        super(context);
        this.name(nombre);
    }
}

SVG.on(document, 'DOMContentLoaded', function() {

    let keeper,
        fullbackLeft,
        centerBackLeft,
        centerBackRight,
        fullbackRight,
        midfielderLeft,
        defensiveMidfielder,
        midfielderRight,
        leftForward,
        rightForward,
        striker;

    draw = SVG('cancha');

    draw.attr('viewBox', '0 0 1030 697');

    Circle.toDataURL(base_url + 'images/canchita.png', function (dataUrl) {
        draw
            .image(dataUrl)
            .attr('class', 'image');

        keeper = new Player('', draw);
        keeper
            .position(KEEPER)
            .draw()

        fullbackLeft = new Player('', draw);
        fullbackLeft
            .position(FULLBACK_LEFT)
            .draw();

        centerBackLeft = new Player('', draw);
        centerBackLeft
            .position(CENTER_BACK_LEFT)
            .draw();

        centerBackRight = new Player('', draw);
        centerBackRight
            .position(CENTER_BACK_RIGHT)
            .draw();

        fullbackRight = new Player('', draw);
        fullbackRight
            .position(FULLBACK_RIGHT)
            .draw();

        midfielderLeft = new Player('', draw);
        midfielderLeft
            .position(LEFT_MIDFIELDER)
            .draw();

        defensiveMidfielder = new Player('', draw);
        defensiveMidfielder
            .position(DEFENSIVE_MIDFIELDER)
            .draw();

        midfielderRight = new Player('', draw);
        midfielderRight
            .position(RIGHT_MIDFIELDER)
            .draw();

        leftForward = new Player('', draw);
        leftForward
            .position(LEFT_FORWARD)
            .draw();

        rightForward = new Player('', draw);
        rightForward
            .position(RIGHT_FORWARD)
            .draw();

        striker = new Player('', draw);
        striker
            .position(STRIKER)
            .draw();
    });


    window.document.getElementById('firstPosition').onclick = function (e) {
        e.preventDefault();

        centerBackLeft.move(CENTER_BACK_LEFT);
        centerBackRight.move(CENTER_BACK_RIGHT);
        leftForward.move(LEFT_FORWARD);
        rightForward.move(RIGHT_FORWARD);
        striker.move(STRIKER);
        midfielderLeft.move(LEFT_MIDFIELDER);
        defensiveMidfielder.move(DEFENSIVE_MIDFIELDER);
        midfielderRight.move(RIGHT_MIDFIELDER);
        striker.move(STRIKER);
    };

    window.document.getElementById('secondPosition').onclick = function (e) {
        e.preventDefault();

        centerBackLeft.move(CENTER_BACK_LEFT);
        centerBackRight.move(CENTER_BACK_RIGHT);
        midfielderLeft.move(LEFT_MIDFIELDER);
        defensiveMidfielder.move(DEFENSIVE_MIDFIELDER);
        midfielderRight.move(RIGHT_MIDFIELDER);

        leftForward.move({
            x: 700,
            y: 200
        });

        rightForward.move({
            x: 700,
            y: 430
        });

        striker.move({
            x: 550,
            y: 310
        });
    };

    window.document.getElementById('thirdPosition').onclick = function (e) {
        e.preventDefault();

        centerBackLeft.move(CENTER_BACK_LEFT);
        centerBackRight.move(CENTER_BACK_RIGHT);

        midfielderLeft.move({
            x: midfielderLeft.x - 20,
            y: midfielderLeft.y + 50,
        });

        defensiveMidfielder.move({
            x: defensiveMidfielder.x,
            y: defensiveMidfielder.y + 110,
        });

        midfielderRight.move({
            x: midfielderRight.x + 160,
            y: midfielderRight.y - 163,
        });

        striker.move({
            x: striker.x + 50,
            y: striker.y,
        });

        leftForward.move({
            x: leftForward.x + 50,
            y: leftForward.y + 50,
        });

        rightForward.move({
            x: rightForward.x + 50,
            y: rightForward.y - 30,
        });
    };

    window.document.getElementById('fourPosition').onclick = function (e) {
        e.preventDefault();

        centerBackLeft.move({
            x: centerBackLeft.x + 10,
            y: centerBackLeft.y - 50
        });

        centerBackRight.move({
            x: centerBackRight.x + 10,
            y: centerBackRight.y + 30
        });

        midfielderLeft.move({
            x: defensiveMidfielder.x - 215,
            y: defensiveMidfielder.y,
        });    

        midfielderRight.move({
            x: midfielderRight.x + 80,
            y: midfielderRight.y - 40,
        });    

        defensiveMidfielder.move({
            x: defensiveMidfielder.x + 20,
            y: defensiveMidfielder.y,
        });    

        /*leftForward.move({
            x: leftForward.x - 200,
            y: leftForward.y - 50,
        });*/

        leftForward.move({
            x: defensiveMidfielder.x + 120,
            y: defensiveMidfielder.y - 110,
        });

        rightForward.move({
            x: rightForward.x + 100,
            y: rightForward.y - 95,
        });

        striker.move({
            x: striker.x,
            y: striker.y - 70,
        });

    };

});