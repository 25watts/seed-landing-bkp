<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{hash?}', 'HomeController@index');

Auth::routes();

Route::post('contact/store', 'HomeController@contact_store')->name('contact.store');

Route::post('image/store', 'HomeController@image_store')->name('image.store');

Route::get('view', 'HomeController@get_view')->name('get.view');