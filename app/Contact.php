<?php
/**
 * Created by PhpStorm.
 * User: puesto6
 * Date: 11/06/18
 * Time: 16:42
 */

namespace Mundial;


use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $table = 'contact';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'email'
    ];

}