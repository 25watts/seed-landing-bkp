<?php

namespace Mundial\Http\Controllers;

use Domain\Contact;
use Domain\Repositories\ContactRepository;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mundial\Infrastructure\DoctrineContactRepository;

class AdminController extends Controller
{

    private $contacts;

    /**
     * AdminController constructor.
     * @param ContactRepository $contacts
     */
    public function __construct(ContactRepository $contacts)
    {
        $this->contacts = $contacts;

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = $this->contacts->paginateAll($perPage = 20, $pageName = 'page');

        return view('home', [
            'contacts' => $contacts
        ]);
    }

    public function export()
    {

        $contacts = $this->contacts->findAll();

        Excel::create(env('APP_NAME'), function($excel) use ($contacts) {


            $excel->setTitle(env('APP_NAME'));

            $excel->sheet('Sheetname', function($sheet) use($contacts) {

                $sheet->loadView('export')->with('contacts', $contacts);

            });

        })->download('xls');

    }

}
