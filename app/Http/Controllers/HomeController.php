<?php

namespace Mundial\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Imagick;
use Mundial\Contact;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $jugadores = [
            'arqueros' => [
                'Armani', 'Caballero', 'Guzman'
            ],
            'defensores' => [
                'Mercado', 'Ansaldi', 'Otamendi', 'Fazio', 'Rojo', 'Tagliafico', 'Acuña'
            ],
            'volantes' => [
                'Mascherano', 'Salvio', 'Biglia', 'Lo Celso', 'Banega', 'Enzo Perez', 'Meza', 'Di Maria', 'Pavon'
            ],
            'delanteros' => [
                'Messi', 'Dybala', 'Higuain', 'Aguero'
            ]
        ];

        if ( ! empty($request->hash))
        {
            try {
                //decrypt($request->hash)
                $contact = Contact::where('email', decrypt($request->hash))->first();
            } catch (\Exception $e)
            {
                //dd($e->getMessage());
            }
        }

        return view('landing', [
            'image' =>  isset($contact) ? $contact->image :  '',
            'jugadores' => $jugadores,
            'contact' => isset($contact) ? $contact : null
        ]);
    }

    /**
     * @param Request $request
     */
    public function contact_store(Request $request)
    {
        $contact = new Contact();

        $contact->image = session('image');
        $contact->email = $request->email;

        $contact->save();

        return new JsonResponse([
            'url' => url('/' . encrypt($contact->email))
        ]);
    }

    public function image_store(Request $request)
    {
        $explode = explode('image=', $request->getContent());

        $image_base64 = urldecode($explode[1]);

        $file_name = 'image_'.time().'.png';

        @list($type, $image_base64) = explode(';', $image_base64);
        @list(, $image_base64) = explode(',', $image_base64);

        if($image_base64 != "") { // storing image in storage/app/public Folder
            Storage::disk('public')->put($file_name,base64_decode($image_base64));
        }

        session(['image' => 'storage/' . $file_name]);

        return new JsonResponse([
            'path' => url('storage/' . $file_name)
        ]);
    }

    public function get_view()
    {

        return view('share_view', [
            "image" => Contact::all()->last()->image
        ]);
    }

}
