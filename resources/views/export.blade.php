<html>

<table class="table">
    <thead>
    <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Apellido</th>
        <th scope="col">Email</th>
        <th scope="col">Teléfono</th>
        <th>Ciudad</th>
        <th>Paquete favorito</th>
        <th>Como nos conocio</th>
        <th>Cada cuanto nos visita</th>
    </tr>
    </thead>
    <tbody>
    @foreach($contacts as $contact)
        <tr>
            <td scope="row">{{ $contact->getFirstName() }}</td>
            <td>{{ $contact->getLastName() }}</td>
            <td>{{ $contact->getEmail() }}</td>
            <td>{{ $contact->getPhone() }}</td>
            <td>{{ $contact->getCity() }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

</html>