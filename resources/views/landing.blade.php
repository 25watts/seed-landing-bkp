<!DOCTYPE html>
<html lang="es">
<head>

    <title>Templo del Fútbol</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="fb:app_id" content="2060866897565447">
    <meta property="og:locale" content="es_AR">
    <meta property="og:title" content="El Templo | Sampaoli acá esta el equipo titular.">
    <meta property="og:site_name" content="Mundial">
    <meta property="og:image" content="{{ asset('images/TF_AccionMundial_shareFB.png') }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:description" content="Estos son mis 11 titulares de la selección. ¡Vamos Argentina!">

    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600" rel="stylesheet" media="bogus">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/screen.css') }}">
    <link rel="icon" href="https://www.templodelfutbol.com.ar/media/favicon/websites/3/favicon.png" type="image/x-icon" />
    <link rel="shortcut icon" href="https://www.templodelfutbol.com.ar/media/favicon/websites/3/favicon.png" type="image/x-icon" />

    <style>
        .tab-content ul li {
            margin: 13px;
        }

        #loading img {
            width: 100px;
        }

    </style>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.6.5/svg.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.5/canvg.min.js"></script>


</head>

<body id="bd" class=" cms-page-view cms-adidas">

<div class="loader" id="loader">
    <svg viewBox="0 0 32 32" width="32" height="32">
        <circle id="spinner" cx="16" cy="16" r="14" fill="none"></circle>
    </svg>
</div>

<header>
    <div class="container-fluid">
        <a title="Templo del Fútbol" href="https://www.templodelfutbol.com.ar/">
            <img src="https://www.templodelfutbol.com.ar/skin/frontend/sns_sport/templodelfutbol/images/logo.svg" alt="Templo del Fútbol" />
        </a>
        <a href="https://www.templodelfutbol.com.ar/"><p> < Volver <span>al inicio</span></p></a>
    </div>
</header>

<section class="header-bg" data-stellar-background-ratio="0.7">
    <div class="container">
        <div class="txt">
            <h1>armá tus <span>11 titulares</span> de la selección</h1><br>
            <h2>Elegí el esquema táctico y definí los jugadores que debería poner Sampaoli en el primer partido del Mundial.</h2>
        </div>
    </div>
</section>

<div class="container">
    <nav class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li><a href="https://www.templodelfutbol.com.ar">Inicio </a></li>
            <li class="active"> Acción mundial</li>
        </ol>
    </nav>
</div>

<section class="esquema">
    <div class="container">

        @if(is_null($contact))
            <p>Elegí el esquema táctico:</p>
            <ul class="nav nav-pills">
                <li role="presentation" class="chose-formation active"><a href="#" id="firstPosition">4-3-3</a></li>
                <li role="presentation" class="chose-formation"><a href="#" id="secondPosition">4-3-1-2</a></li>
                <li role="presentation" class="chose-formation"><a href="#" id="thirdPosition">4-2-3-1</a></li>
                <li role="presentation" class="chose-formation"><a href="#" id="fourPosition">5-3-2</a></li>
            </ul>
        @endif

        <!--<a href="" class="btn-share" id="share"><i class="ion-android-share-alt" aria-hidden="true"></i></a>-->

        <div class="field-container">
            
            @if(is_null($contact))
            
                <div id="cancha">
    
                    <div id="chose" class="modal chose">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs modal-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#arquero" aria-controls="arquero" role="tab" data-toggle="tab">Arquero</a></li>
                                        <li role="presentation"><a href="#defensor" aria-controls="defensor" role="tab" data-toggle="tab">Defensor</a></li>
                                        <li role="presentation"><a href="#mediocampista" aria-controls="mediocampista" role="tab" data-toggle="tab">Mediocampista </a></li>
                                        <li role="presentation"><a href="#delantero" aria-controls="delantero" role="tab" data-toggle="tab">Delantero</a></li>
                                    </ul>
                                    <!-- Nav tabs -->
                                </div>
                                <div class="modal-body">
    
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="arquero">
                                            <div class="content-players">
                                                @foreach($jugadores['arqueros'] as $arquero)
                                                    <a href="#" class="chose-player" data-name="{{ $arquero }}">
                                                        <div class="player">
                                                            <div class="avatar">
                                                                <img src="{{ url("jugadores/" . str_replace(' ', '', $arquero) . ".png") }}" alt="">
                                                            </div>
                                                            <div class="player-name">
                                                                <p>{{ $arquero }}</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            </div><!-- end tab 1 -->
                                        </div>
    
                                        <div role="tabpanel" class="tab-pane" id="defensor">
                                            <div class="content-players">
                                                @foreach($jugadores['defensores'] as $defensor)
                                                    <a href="#" class="chose-player" data-name="{{ $defensor == 'Acuña' ? 'Acunia' : $defensor }}">
                                                        <div class="player">
                                                            <div class="avatar">
                                                                <img src="{{ url("jugadores/" . str_replace(' ', '', $defensor == 'Acuña' ? 'Acunia' : $defensor) . ".png") }}" alt="">
                                                            </div>
                                                            <div class="player-name">
                                                                <p>{{ $defensor }}</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            </div> <!-- end content player -->
                                        </div>
    
                                        <div role="tabpanel" class="tab-pane" id="mediocampista">
                                            <div class="content-players">
                                                @foreach($jugadores['volantes'] as $volante)
                                                    <a href="#" class="chose-player" data-name="{{ $volante }}">
                                                        <div class="player">
                                                            <div class="avatar">
                                                                <img src="{{ url("jugadores/" . str_replace(' ', '', $volante) . ".png") }}" alt="">
                                                            </div>
                                                            <div class="player-name">
                                                                <p>{{ $volante }}</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            </div> <!-- end content player -->
                                        </div>
    
                                        <div role="tabpanel" class="tab-pane" id="delantero">
                                            <div class="content-players">
                                                @foreach($jugadores['delanteros'] as $delantero)
                                                    <a href="#" class="chose-player" data-name="{{ $delantero }}">
                                                        <div class="player">
                                                            <div class="avatar">
                                                                <img src="{{ url("jugadores/" . str_replace(' ', '', $delantero) . ".png") }}" alt="">
                                                            </div>
                                                            <div class="player-name">
                                                                <p>{{ $delantero }}</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                @endforeach
                                            </div> <!-- end content player -->
                                        </div>
                                    </div>
    
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <div id="loading">
                                    <img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            @else

                <img src="{{ url($contact->image) }}" alt="">
            
            @endif
        </div>

    </div>
</section>

<!-- Modal terms -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-terms">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header large">
                <h6>BASES Y <span>CONDICIONES</span></h6>
                <button type="button" class="close secondary-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    1) Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.<br><br>
                    2) eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                    voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. <br><br>
                    3) Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.

                </p>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>


<!-- Modal Awards -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-share">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header large">
                <h6>tus <span>11 titulares </span> de la selección</h6>
                <button type="button" class="close secondary-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Ya definiste tus 11 titulares de la selección, <br> compartí en Facebook y para participá <br>de los premios.
                </p>
                <img src="{{ asset('img/award.png') }}" class="modal-img" alt="Templo del Fútbol">
                <p>
                    <span>PREMIOS</span><br>
                    1er: Camiseta Selección Argentina <br>
                    2do: Pelota Mundial<br>
                    3ro: Mochila Adidas<br>
                    4to: Mochila Adidas

                </p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn-share" id="share">COMPARTIR</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal Awards -->
<div class="modal" tabindex="-1" role="dialog" id="modal-awards">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header large">
                <h6>PREMIOS</h6>
                <button type="button" class="close secondary-modal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{ asset('img/award.png') }}" class="modal-img" alt="Templo del Fútbol">
                <p>
                    <span>PREMIOS</span><br>
                    1er: Camiseta Selección Argentina <br>
                    2do: Pelota Mundial<br>
                    3ro: Mochila Adidas<br>
                    4to: Mochila Adidas

                </p>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>


<section class="legal-terms">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="items">
                    <a data-toggle="modal" data-target="#modal-terms"> Bases y condiciones</a>
                    <a data-toggle="modal" data-target="#modal-awards"> Premios</a>
                </div>
            </div>
        </div>
    </div>
</section>


<div id="sns_footer_bottom">
    <div class="container-fluid">
        <div class="sns-copyright">
            © 2018 Templo del Fútbol - Todos los derechos reservados.
        </div>
        <div class="block-owner">
            <a href="https://www.25watts.com.ar" target="_blank"><img src="https://www.templodelfutbol.com.ar/skin/frontend/sns_sport/templodelfutbol/images/logo25.svg" alt="25Watts"></a>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/stellar.min.js') }}"></script>

<script src="{{ asset('js/front/positions.js') }}"></script>
<script src="{{ asset('js/front/app.js') }}"></script>


<script>


    $(function () {
        //$('#modal-share').modal('show');

        $('#loader').hide();

        window.fbAsyncInit = function() {

            FB.init({
                appId: 2060866897565447,
                cookie: true,
                xfbml: true,
                version: 'v3.0'
            });

            FB.AppEvents.logPageView();

            FB.getLoginStatus(function (response) {
                console.log(response);
                //statusChangeCallback(response);
            });

            FB.getLoginStatus(handleSessionResponse);


            $('.chose-formation').on('click', function (e) {
                $('.chose-formation').removeClass('active');
                $(this).addClass('active');
            });


            $('#modal-share').on('shown.bs.modal', function () {

                $('#share')
                    .click(function (e) {

                        $('#loader').show();

                        e.preventDefault();

                        let svg = document.getElementById("SvgjsSvg1006");
                        let svgData = new XMLSerializer().serializeToString( svg );

                        let canvas = document.createElement( "canvas" );
                        canvas.width = 1220;
                        canvas.height = 700;
                        let ctx = canvas.getContext( "2d" );

                        let img = document.createElement( "img" );
                        img.setAttribute( "src", "data:image/svg+xml;base64," + btoa( svgData ) );
                        img.setAttribute("width", 700);

                        img.onload = function () {

                            ctx.drawImage( img, 0, 0 );
                            let png = canvas.toDataURL("image/png");

                            $.ajax({
                                url: "{{ route('image.store') }}",
                                type: "post",
                                dataType: "json",
                                headers: {
                                    'Cache-Control': "no-cache",
                                    'Content-Type': "application/json"
                                },
                                data: {
                                    '_method': "POST",
                                    'image': png
                                },
                                success: function (data) {
                                    $('#loader').hide();

                                    FB.login(function(response) {


                                        FB.api('/me?fields=email', response.accessToken, function(data) {

                                            $.post("{{ route('contact.store') }}", {
                                                '_token': "{{ csrf_token() }}",
                                                'email': data.email
                                            }, function (data2) {

                                                if (response.authResponse) {

                                                    console.log(data2);

                                                    /**
                                                     * acá haria el share
                                                     */
                                                    FB.ui({
                                                        method: 'share',
                                                        href: data2.url,
                                                    }, function(data) {

                                                    });

                                                } else {
                                                    console.log("user did not give permission");
                                                }
                                            }, 'json');

                                        }, {
                                            scope:'email'
                                        });

                                    });
                                },
                                error: function (data) {
                                    console.log(data)
                                }
                            });

                        };

                    });
            });
        };

        handleSessionResponse = function (response) {

            FB.logout(handleSessionResponse(response));

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    });


</script>

<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600" rel="stylesheet">