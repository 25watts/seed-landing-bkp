<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Neverland">
    <meta name="description" content="En Neverland la vuelta al cole es super divertida con juegos para los chicos. Aprovechá la promo 2x1 en nuestros parques de todo el país.  Estamos en 11 provincias. Además, preparamos kits escolares descargables para vos. Descargá gratis nuestros kits para colorear, recortar y más. Participá del concurso y ganá pases para divertirte en nuestro parque.">
    <meta name="keywords" content="Neverland, parque de diversiones, juegos para niños, que hacer con niños, jueguitos, cordoba, buenos aires, capital federal, catamarca, chubut, mendoza, neuquen, salta, san juan, santa fe, santiago del estero, tucuman, cordoba shopping, nuevocentro shopping, Al Oeste Shopping, Carrefour San Fernando,  Carrefour San Martín, Carrefour Tandil, Nordelta, Palmas del Pilar Shopping, Terrazas de Mayo, Abasto Shopping, Alcorta Shopping, Dot Baires Shopping, Alto del Solar, Portal Trelew, Paseo de la Ribera, Paseo Rivera Indarte, Paseo Libertad Lugones, Shopping Río, Villa Allende Shopping, Mendoza Plaza Shopping, Alto Comahue Shopping, Portal Patagonia, Alto Noa Shopping, Portal Salta Shopping, Patio Alvear Shopping, Paseo Libertad, Portal Rosario, Portal Santiago, Portal Tucumán Shopping, colorear, descarga gratis juegos, festejar cumpleaños, salon para cumpleaños chicos">

    <title>Error - La vuelta al cole con Neverland es super divertida. Conocé la promo 2x1 en tu parque más cercano.</title>

    <meta property="og:title" content="La vuelta al cole con Neverland es super divertida. Conocé la promo 2x1 en tu parque más cercano." />
    <meta property="og:description" content="En Neverland la vuelta al cole es super divertida con juegos para los chicos. Aprovechá la promo 2x1 en nuestros parques de todo el país.  Estamos en 11 provincias. Además, preparamos kits escolares descargables para vos. Descargá gratis nuestros kits para colorear, recortar y más. Participá del concurso y ganá pases para divertirte en nuestro parque." />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('images/og.png') }}" />

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400,600,700,800,9000" rel="stylesheet" media="bogus">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/screen.css') }}">

    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>

</head>

<body>

<nav class="navbar nav-main" id="navMain">
    <div class="container">
    
        <div class="navbar-header">
            <a class="navbar-brand back-to-top" href="#">
                <img src="{{ asset('images/logo.svg') }}" data-white-src="images/logo.svg" data-color-src="images/logo-color.svg" alt="Neverland" class="visible-md visible-lg">
                <img src="{{ asset('images/logo-color.svg') }}" alt="Neverland" class="visible-xs visible-sm">
            </a>
        </div>

    </div>
</nav>


<section class="response-page">
    <div class="form-response">
        <div class="content">
            <h2>
                ¡Oops!<br>
                <span>Hubo un error al enviar el mensaje</span>
            </h2>
            <ul>
                <li><a href="https://www.facebook.com/Neverlandweb/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/neverlandweb/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://twitter.com/NeverlandWeb" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.youtube.com/user/NeverlandWeb" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
    </div>
</section>

<footer id="footerMain">

<div class="container">
    <div class="main-footer">
        <div class="logo">
            <img src="{{ asset('images/logo.svg') }}" alt="Neverland">
        </div>
        <div class="social">
            <ul>
                <li>Te esperamos en:</li>
                <li><a href="https://www.facebook.com/Neverlandweb/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/neverlandweb/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <li><a href="https://twitter.com/NeverlandWeb" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.youtube.com/user/NeverlandWeb" target="_blank"><i class="fa fa-youtube"></i></a></li>
            </ul>
        </div>
    </div>

    <div class="copyright">
        <p>© 2017 Todos los derechos reservados. <span>Realizado por: <a href="http://www.25watts.com.ar" target="_blank"><img src="{{ asset('images/logo_25.svg') }}" alt="25Watts"></a></span></p>
    </div>
</div>

</footer>



<script src="{{ asset('js/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script>

    $(document).ready(function(){
        var windowWidth = $(window).width();
        var $menuNav = $('#navMain');
        var menuNavHeight = $menuNav.outerHeight();
        var useFixedMenu = $(document).scrollTop() > menuNavHeight;
        var whiteLogo = $('.navbar-brand img').data('white-src');
        var colorLogo = $('.navbar-brand img').data('color-src');

        // NAVBAR MOBILE
        function navScript(){
            if(windowWidth >= 992){
                if($(document).scrollTop() > menuNavHeight){
                    $menuNav.addClass('navbar-mobile');
                    $('.navbar-brand img').attr('src', colorLogo);
                }else{
                    $menuNav.removeClass('navbar-mobile');
                    $('.navbar-brand img').attr('src', whiteLogo);
                }
            }else{
                $menuNav.addClass('navbar-mobile');
            }
        }
        navScript();

        $(document).scroll(function() {
            navScript();
        });
        $('#navbar-toggle').click(function(){
            $(this).toggleClass('hamburger-animation');
        });

    });
</script>
</body>
</html>
