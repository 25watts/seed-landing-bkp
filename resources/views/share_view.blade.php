<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta property="fb:app_id" content="2060866897565447">
    <meta property="og:locale" content="es_AR">
    <meta property="og:title" content="El Templo | La otra mirada">
    <meta property="og:site_name" content="Mundial">
    <meta property="og:type" content="website">
    <meta property="og:image" content="{{ url($image) }}">
    <meta property="og:url" content="https://mundial.ideasconluzpropia.com.ar/">
    <meta property="og:description" content="Notas focalizadas en el quehacer político de la Argentina.">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/2.6.5/svg.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.5/canvg.min.js"></script>

    <title>El Templo del Futbol | Mundial</title>


</head>
<body>

    <a href="#" id="share">share</a>

    <script src="{{ asset('js/front/positions.js') }}"></script>
    <script src="{{ asset('js/front/app.js') }}"></script>
    <script>
        window.fbAsyncInit = function() {

            FB.init({
                appId      : 2060866897565447,
                cookie     : true,
                xfbml      : true,
                version    : 'v3.0'
            });

            FB.AppEvents.logPageView();

            FB.getLoginStatus(function(response) {
                console.log(response);
                //statusChangeCallback(response);
            });

            $('#share').on('click', function () {

                FB.login(function(response) {
                    if (response.authResponse) {

                        /**
                         * acá haria el share 
                         */
                        FB.ui({
                            method: 'share',
                            href: "{{ route('get.view') }}",
                        }, function(response){
                            console.log(response);
                        });


                    } else {
                        console.log("user did not give permission");
                    }
                }, {
                    scope:'email'
                });
            });

        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

</body>
</html>