@extends('layouts.app')

@section('content')
<div >
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('export') }}" class="btn btn-success pull-right">Exportar a PDF</a>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellido</th>
                            <th scope="col">Email</th>
                            <th scope="col">Teléfono</th>
                            <th>Ciudad</th>
                            <th>Paquete favorito</th>
                            <th>Como nos conocio</th>
                            <th>Cada cuanto nos visita</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($contacts as $contact)
                                <tr>
                                    <th scope="row">{{ $contact->getFirstName() }}</th>
                                    <td>{{ $contact->getLastName() }}</td>
                                    <td>{{ $contact->getEmail() }}</td>
                                    <td>{{ $contact->getPhone() }}</td>
                                    <td>{{ $contact->getCity() }}</td>
                                    <td>{{ $contact->getPack() }}</td>
                                    <td>{{ $contact->getFoundOut() }}</td>
                                    <td>{{ $contact->getOftenTime() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>


                        <div class="pull-right">{{ $contacts->render() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
